const { response } = require("express");
const userService = require("../service/user-service");
const UserService = require("../service/user-service");

class userController {
  async registration(request, response, next) {
    try {
      const { email, password } = request.body;
      const userData = await userService.registration(email, password);
      response.cookie("refreshToken", userData.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true
      });
      return response.json(userData);
    } catch (error) {
      console.log(error);
    }
  }

  async login(request, respose, next) {
    try {
    } catch (error) {}
  }

  async logout(request, respose, next) {
    try {
    } catch (error) {}
  }

  async getUsers(request, respose, next) {
    try {
    } catch (error) {}
  }

  async activate(request, respose, next) {
    try {
    } catch (error) {}
  }

  async refresh(request, respose, next) {
    try {
    } catch (error) {}
  }
}

module.exports = new userController();
