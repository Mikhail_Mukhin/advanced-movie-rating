import { combineReducers, createStore } from "redux";
import { authenticationReducer } from "./auth";

const rootReducer = combineReducers({
    authentication: authenticationReducer
});

export const store = createStore(rootReducer);
