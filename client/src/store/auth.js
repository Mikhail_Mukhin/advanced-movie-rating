const auth = {
  authenticated: true
};

export const authenticationReducer = (state = auth, action) => {
  switch (action.type) {
    default:
      return state.authenticated;
  }
};
