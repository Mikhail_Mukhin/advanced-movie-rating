import React from "react";
import "./styles/main.scss";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Estimates from "./components/Tabs/Estimates";
import Films from "./components/Tabs/Films";
import Friends from "./components/Tabs/Friends";
import Profile from "./components/Tabs/Profile";
import Tabs from "./components/Tabs/Tabs";
import Login from "./components/Login";

function App() {
  const authenticated = useSelector((state) => state.authentication);
  console.log(authenticated);

  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route
            path="/profile"
            element={
              <>
                <Tabs></Tabs>
                <Profile />
              </>
            }
          />
          <Route
            path="/estimates"
            element={
              <>
                <Tabs></Tabs>
                <Estimates />
              </>
            }
          />
          <Route
            path="/films"
            element={
              <>
                <Tabs></Tabs>
                <Films />
              </>
            }
          />
          <Route
            path="/friends"
            element={
              <>
                <Tabs></Tabs>
                <Friends />
              </>
            }
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
