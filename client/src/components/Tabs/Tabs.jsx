import React, { useState } from "react";
import { NavLink } from "react-router-dom";

function Tabs() {
  const [active, setActive] = useState(null);
  const tabs = {
    profile: "Профиль",
    estimates: "Оценки",
    friends: "Друзья",
    films: "Фильмы"
  };
  const makeActiveTab = (tabName) => setActive(tabName);

  return (
    <div className="tabs">
      {Object.entries(tabs).map((tab) => (
        <NavLink
          key={tab[0]}
          onClick={() => makeActiveTab(tab[0])}
          className={`${active === tab[0] ? `active-tab ${tab[0]}` : tab[0]}`}
          to={`/${tab[0]}`}
        >
          {tab[1]}{" "}
        </NavLink>
      ))}
    </div>
  );
}

export default Tabs;
