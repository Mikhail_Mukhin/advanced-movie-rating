import React from "react";
import UserName from "./UserName";
import SearchMovies from "../SearchMovies";
import UserEstimates from "../charts/UserEstimates";
import EstimatesDistribution from "../charts/EstimatesDistribution";
import GenreDistribution from "../charts/GenreDistribution";
import CountryDistribution from "../charts/CountryDistribution";
import YearsDistribution from "../charts/YearsDistribution";
import EstimatesHistory from "./EstimatesHistory";

function Estimates() {
  return (
    <div className="estimates-content">
      <UserName />
      <SearchMovies />
      <div className="estimates-content__genre-list">список жанров</div>
      <div className="estimates-content__statistics">
        тут будет статистика по оценкам
      </div>
      <UserEstimates />
      <EstimatesDistribution />
      <GenreDistribution />
      <CountryDistribution />
      <YearsDistribution />
      <div className="estimates-content__top-personalities">
        тут будет топ актёров, режисеров
      </div>
      <EstimatesHistory />
    </div>
  );
}

export default Estimates;
