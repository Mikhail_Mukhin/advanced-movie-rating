import React from "react";
import UserName from "./UserName";
import FriendEstimates from "../charts/FriendEstimates";
import FriendsEstimatesHistory from "./FriendsEstimatesHistory";

function Friends() {
  return (
    <div className="friends-content">
      <UserName />
      <div className="friends-content__friends-list">
        тут будет список друзей
      </div>
      <FriendEstimates />
      <FriendsEstimatesHistory />
    </div>
  );
}

export default Friends;
