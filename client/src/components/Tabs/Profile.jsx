import React from "react";
import UserName from "./UserName";
import UserEstimates from "../charts/UserEstimates";
import FriendEstimates from "../charts/FriendEstimates";

function Profile() {
  return (
    <div className="profile-content">
      <UserName />
      <div className="profile-content__signature"> подпись</div>
      <div className="profile-content__registration-date">дата регистрации</div>
      <div className="profile-content__note">заметка</div>
      <div className="profile-content__friend">друзья</div>
      <div className="statistic-block">
        <div className="statistic-block__films">11</div>
        <div className="statistic-block__TV-series">11</div>
        <div className="statistic-block__TV-series">11</div>
        <div className="statistic-block__friens">11</div>
        <div className="statistic-block__favorite-films">11</div>
        <div className="statistic-block__expectfe-movies">11</div>
      </div>
      <UserEstimates />
      <FriendEstimates />
    </div>
  );
}

export default Profile;
