import React from "react";
import UserName from "./UserName";
import WillWatch from "./WillWatch";

function Films() {
  return (
    <div className="films-content">
      <UserName />
      <div className="favorite-films">тут будет список любимых фильмов</div>
      <div className="will-watch">
        <div className="will-watch">тут будет список "буду смотеть"</div>
        <WillWatch />
      </div>
    </div>
  );
}

export default Films;
