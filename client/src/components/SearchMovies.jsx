import React, { useState, useEffect } from "react";
import Select from "react-select";

function SearchMovie() {
  const [film, setFilm] = useState("");
  const [movieList, setMovieList] = useState([]);

  const change = (value) => {
    async function getFilms(value) {
      try {
        const response = await fetch(
          `https://api.themoviedb.org/3/search/movie?api_key=0aecf6db407ccb300d17f65b602b98c5&query=${value}`
        );
        const data = await response.json();
        setMovieList(
          data.results.map((film) => {
            return { value: film.original_title, label: film.original_title };
          })
        );
      } catch (e) {
        console.log(e);
      }
    }
    getFilms(value);
  };

  return (
    <div className="main-page">
      <Select options={movieList} onInputChange={change} />
    </div>
  );
}

export default SearchMovie;
