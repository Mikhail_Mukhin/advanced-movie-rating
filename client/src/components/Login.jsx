import React, { useState } from "react";
import { useNavigate } from "react-router";

function Login() {
  const navigate = useNavigate();
  const [data, setData] = useState({
    username: "",
    password: ""
  });

  const { username, password } = data;

  const changeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (!!data.username && !!data.password) {
      navigate("/profile");
    }
  };
  return (
    <div className="login">
      <form className="submit-form" onSubmit={submitHandler}>
        <input
          className="username"
          type="text"
          name="username"
          value={username}
          onChange={changeHandler}
        />
        <br />
        <input
          className="password"
          type="password"
          name="password"
          value={password}
          onChange={changeHandler}
        />
        <br />
        <input className="submit" type="submit" name="submit" value="Войти" />
      </form>
    </div>
  );
}

export default Login;
